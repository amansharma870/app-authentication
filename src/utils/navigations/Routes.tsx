import * as React from 'react';
import SignUp from '../../screens/SignUp';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Login from '../../screens/Login';
import Home from '../../screens/Home';
import {useEffect, useState} from 'react';
import Loading from '../../screens/Loading';
import AsyncStorage from '@react-native-async-storage/async-storage';
const Stack = createNativeStackNavigator();

export enum ScreenNames {
  SignUp = 'SignUp',
  AccountScreen = 'AccountScreen',
  Login = 'Login',
  Home = 'Home',
  Loading = 'Loading',
}

export const authStack = () => {
  const [isLoggedIn, setLogged] = useState(null);

  useEffect(() => {
    async function fetchMyAPI() {
      const token = await AsyncStorage.getItem('token');
      if (token) {
        setLogged(true);
      } else {
        setLogged(false);
      }
    }

    fetchMyAPI();
  }, []);
  return (
    <Stack.Navigator initialRouteName={ScreenNames.SignUp}>
      {isLoggedIn == null ? (
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name={ScreenNames.Loading}
          component={Loading}
        />
      ) : isLoggedIn == true ? (
        <Stack.Screen
          options={{
            headerShown: false,
          }}
          name={ScreenNames.Home}
          component={Home}
        />
      ) : (
        <>
          <Stack.Screen
            options={{
              headerShown: false,
            }}
            name={ScreenNames.SignUp}
            component={SignUp}
          />
          <Stack.Screen
            options={{
              headerShown: false,
            }}
            name={ScreenNames.Login}
            component={Login}
          />
        </>
      )}
    </Stack.Navigator>
  );
};
