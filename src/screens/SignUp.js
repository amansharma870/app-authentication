import React, {useState} from 'react';
import {
  Text,
  View,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import Container from '../components/Container';
import {Button, Avatar, TextInput} from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Home = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const sendCred = async () => {
    fetch('http://10.0.2.2:3000/signup', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then(res => res.json())
      .then(async data => {
        try {
          await AsyncStorage.setItem('token', data.token);
        } catch (e) {
          console.log(e);
        }
      });
  };
  return (
    <ScrollView>
      <Container>
        <KeyboardAvoidingView behavior="position">
          <StatusBar backgroundColor="blue" barStyle="light-content" />

          <View style={{alignSelf: 'center', marginTop: 8}}>
            <Avatar.Image
              style={{alignSelf: 'center'}}
              size={150}
              source={require('../assets/img/codewithaman.png')}
            />
            <Text style={{fontSize: 25, fontWeight: 'bold', marginLeft: 14}}>
              Welcome to{' '}
            </Text>
            <Text
              style={{
                fontSize: 25,
                fontWeight: 'bold',
                color: 'blue',
              }}>
              codeWithaman
            </Text>
            <View style={{borderBottomColor: 'blue', borderBottomWidth: 3}} />
          </View>
          <View style={{marginTop: 15}}>
            <Text style={{fontSize: 18, fontWeight: 'bold'}}>
              Create New Account
            </Text>

            <TextInput
              mode="outlined"
              label="Your Email"
              value={email}
              onChangeText={text => setEmail(text)}
              theme={{colors: {primary: 'blue'}}}
              placeholder="Enter your mail"
              right={<TextInput.Affix text="/100" />}
            />
            <TextInput
              mode="outlined"
              label="Your Password"
              value={password}
              secureTextEntry={true}
              onChangeText={text => setPassword(text)}
              theme={{colors: {primary: 'blue'}}}
              placeholder="Enter your password"
              right={<TextInput.Affix text="/100" />}
            />

            <Button
              mode="contained"
              style={{marginBottom: 5, marginTop: 5}}
              onPress={() => sendCred()}>
              SignUp
            </Button>
            <TouchableOpacity>
              <Text
                onPress={() => navigation.navigate('Login')}
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                }}>
                {' '}
                Already have an account?
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </Container>
    </ScrollView>
  );
};

export default Home;
