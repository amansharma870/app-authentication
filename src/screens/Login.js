import React, {useState} from 'react';
import {
  Text,
  View,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  KeyboardAvoidingView,
} from 'react-native';
import {Button, Avatar, TextInput} from 'react-native-paper';
import Container from '../components/Container';
const Login = ({navigation}) => {
  const [text, setText] = React.useState('');

  return (
    <ScrollView>
      <Container>
        <KeyboardAvoidingView behavior="position">
          <StatusBar backgroundColor="blue" barStyle="light-content" />

          <View style={{alignSelf: 'center', marginTop: 8}}>
            <Avatar.Image
              style={{alignSelf: 'center'}}
              size={150}
              source={require('../assets/img/codewithaman.png')}
            />
            <Text style={{fontSize: 25, fontWeight: 'bold', marginLeft: 14}}>
              Welcome to{' '}
            </Text>
            <Text
              style={{
                fontSize: 25,
                fontWeight: 'bold',
                color: 'blue',
              }}>
              codeWithaman
            </Text>
            <View style={{borderBottomColor: 'blue', borderBottomWidth: 3}} />
          </View>
          <View style={{marginTop: 15}}>
            <Text style={{fontSize: 18, fontWeight: 'bold'}}>
              Login with Email
            </Text>

            <TextInput
              mode="outlined"
              label="Your Email"
              theme={{colors: {primary: 'blue'}}}
              placeholder="Enter your mail"
              right={<TextInput.Affix text="/100" />}
            />
            <TextInput
              mode="outlined"
              label="Your Password"
              theme={{colors: {primary: 'blue'}}}
              placeholder="Enter your password"
              right={<TextInput.Affix text="/100" />}
            />

            <Button
              mode="contained"
              style={{marginBottom: 5, marginTop: 5}}
              onPress={() => navigation.navigate('SignUp')}>
              SignUp
            </Button>
            <TouchableOpacity>
              <Text
                onPress={() => navigation.navigate('SignUp')}
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                }}>
                Don't have an account?
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>
      </Container>
    </ScrollView>
  );
};

export default Login;
