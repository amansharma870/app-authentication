import React, {useState} from 'react';
import {
  Text,
  View,
  StatusBar,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import {Button, Avatar, TextInput} from 'react-native-paper';

const Loading = ({navigation}) => {
  return (
    <>
      <ActivityIndicator
        style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}
        size={'large'}
        color={'blue'}
      />
    </>
  );
};

export default Loading;
