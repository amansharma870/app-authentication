import React from 'react';
import {Text, ScrollView} from 'react-native';

const Home = ({navigation}) => {
  return (
    <ScrollView>
      <Text style={{fontSize: 25, fontWeight: 'bold', marginLeft: 14}}>
        Your Email id is xyz
      </Text>
    </ScrollView>
  );
};

export default Home;
