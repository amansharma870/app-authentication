import React from 'react';
import {Navigator} from './src/utils/navigations/Navigator';


const App = () => {
  return (
   <Navigator />
  );
};

export default App;
